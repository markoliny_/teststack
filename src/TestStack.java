import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.EmptyStackException;

public class TestStack<T> {
    private Class<T> clazz;
    private T[] array;
    private int head;
    private int boost = 2;

    private TestStack(Class<T> className){
        clazz = className;
        head = -1;
        array = (T[]) Array.newInstance(clazz, boost);
    }

    private void push(T value) {
        head++;
        if (head+1 > array.length) {
            T[] arr1 = (T[]) Array.newInstance(clazz, array.length + boost);
            System.arraycopy(array, 0, arr1, 0, head);
            array = arr1;
        }
        array[head] = value;
    }

    private T pop(){
        if (head == -1) throw new EmptyStackException();
        return array[head--];
    }

    private void trimToSize() {
        if (head+1 < array.length) {
            array = Arrays.copyOf(array, head+1);
        }
    }

    private void print(){
        System.out.println("Значения в стеке: ");
        for (int i = 0; i < head + 1; i++) {
            System.out.println(array[i]);
        }
        System.out.println("~~~~~~~~~~~~~~~~~~");
        System.out.println();
    }

    private void size(){
        System.out.println("Текущий размер стека: "+(head+1));
        System.out.println();
    }

    public static void main(String[] args) {
        TestStack stack = new TestStack<Character>(Character.class);
        stack.push((char)72);
        stack.push((char)69);
        stack.push((char)76);
        stack.push((char)76);
        stack.push((char)79);
        stack.print();
        stack.size();
        stack.pop();
        stack.print();
        stack.size();
        stack.trimToSize();
        stack.push((char)89);
        stack.push((char)65);
        stack.print();
        stack.pop();
        stack.print();
        stack.size();
        stack.push((char)72);
        stack.print();
        stack.pop();
        stack.print();
        stack.size();
    }
}

